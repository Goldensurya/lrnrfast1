import React from 'react';
import SideNav from '../SideNav/SideNav';


const toolBar = (props) => (
    <header className="Toolbar">
        <SideNav />
    </header>
);

export default toolBar;