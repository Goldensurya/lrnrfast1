import React from 'react';

import CKEditor from '@ckeditor/ckeditor5-react';
import BalloonEditor from '@ckeditor/ckeditor5-build-balloon';


const wysiwgEditor = (props) => (
    <div className="Bodypart-wrapper ml-40r">
        <div className="container custom-container">
            <div className="clearfix"></div>
            <div className="row">
                <div className="col-sm-12 editor">
                    <div className="topic-title">
                        <h4>WYSIWYG Editor</h4>
                    </div>
                    <CKEditor
                        editor={BalloonEditor}
                        data="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release."
                        onChange={(e ,i)=>props.handleChangeText(e, i)}
                        onBlur={ (editor, evt) => props.getAllData(editor, evt)}
                        config={{
                            toolbar: ['Bold', 'Italic', 'Underline', 'Link', 'Undo', 'Redo', 'ImageUpload'],
                            // toolbar: ['Bold', 'Italic', 'Underline', 'Link', 'Undo', 'Redo', 'BulletedList', 'ImageUpload', 'InsertTable', 'InsertMedia', 'ClipBoard'],
                            removePlugins: ['Heading', 'ImageCaption', 'ImageStyle', 'ImageToolbar']
                        }}
                    />
                    {/* <div className="readOnly">
                        <CKEditor
                            disabled
                            editor={BalloonEditor}
                            data={props.datageta}
                            config={{
                                toolbar: [],
                                removePlugins: []
                            }}
                            
                        />
                    </div> */}
                </div>
            </div>
        </div>
    </div>

);
 
export default wysiwgEditor;
