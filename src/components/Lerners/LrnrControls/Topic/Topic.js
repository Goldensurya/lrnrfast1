import React from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import BalloonEditor from '@ckeditor/ckeditor5-build-balloon';


const topic = (props) => (
    <div className="Bodypart-wrapper ml-40r pt-0">
        <div className="container custom-container">
            <div className="clearfix"></div>
            <div className="row">
                <div className="clearfix"></div>
                <div className="col-sm-12 editor">
                    <div className="topic-title">
                    <CKEditor
                        editor={BalloonEditor}
                        data="Topic Name"
                        onBlur={(editor, evt) => props.getAllData(editor, evt)}
                        onChange={(e, i) => props.handleChangeText(e, i)}
                        config={{
                            toolbar: ['Heading','Bold', 'Italic'],
                            removePlugins: ['ImageCaption', 'ImageStyle', 'ImageToolbar']
                        }}
                    />
                    </div>
                    <CKEditor
                        editor={BalloonEditor}
                        data="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
                        onBlur={(editor, evt) => props.getAllData(editor, evt)}
                        onChange={(e, i) => props.handleChangeText(e, i)}
                        config={{
                            toolbar: ['Bold', 'Italic', 'Underline', 'Link', 'Undo', 'Redo', 'ImageUpload'],
                            removePlugins: ['Heading', 'ImageCaption', 'ImageStyle', 'ImageToolbar']
                        }}
                    />
                </div>
            </div>
        </div>
    </div>

);

export default topic;