
import React, { Component } from 'react';
import EventBus from 'vertx3-eventbus-client';

import AddMember from './AddMember/AddMember';
import WysiwgEditor from './WysiwgEditor/WysiwgEditor';
import Topic from './Topic/Topic';
import Widgets from './Widgets/Widgets';

class LrnrControls extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: ''
        }
    }


    componentDidMount() {
        this.eventBus = new EventBus('http://2eaa0829.ngrok.io/eventbus');
        const eb = this.eventBus;
        eb.onopen = function () {
        	console.log(' bus conncected : ');
        }
    }
    handleChangeText = (event, editor) => {
        const data = editor.getData();
        this.setState({data:data})
    }
 
    getAllData = (editor, evt) =>{
        const data = evt.getData();
        const eb = this.eventBus;
        const message = {
             message: data
        };
        eb.publish('editorTextIn', message);
        eb.registerHandler('editorTextOut', (err, message) => {
            if (!err) {
                console.log(' message : ',message);
            }
            else{
                console.log(' err : ',err);
            }
        })
    }


    render() {
        const datageta = this.state.data;
        return (
            <div>
                <AddMember />
                <WysiwgEditor
                    handleChangeText={this.handleChangeText}
                    getAllData={this.getAllData}
                    datageta={datageta}
                />
                <Topic
                    handleChangeText={this.handleChangeText}
                    getAllData={this.getAllData}
                />
                <Widgets />
            </div>
        )
    }
}
export default LrnrControls;