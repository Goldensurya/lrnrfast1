import React from 'react';


const widgets = (props) => (
    <div className="Bodypart-wrapper ml-40r pt-0">
        <div className="container custom-container">
            <div className="row">
                <div className="col-sm-12">
                    <button type="button" title="add widgets" className="add-vid-btn btn btn-lg"><i className="fas fa-plus-circle"></i></button>
                </div>
            </div>
        </div>
    </div>
    

);

export default widgets;