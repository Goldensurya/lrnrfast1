
import React, { Component } from 'react';
import $ from 'jquery';
import axios from 'axios';

class LrnrAll extends Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            parentData: '',
            name: '',
            parent: ''

        }
    }
    componentWillMount() {
        axios.get('http://2eaa0829.ngrok.io/menu', {
            responseType: 'json'
        }).then(response => {
            this.setState({ data: response.data })
        });
    }

    handleGetId = (id) => {
        const parent = id;
        setTimeout(() => {
            this.setState({ parent: parent })
        }, 1000)
    }

    handleChange = (event) => {
        if (event.key == 'Enter') {
            $(".inputBox").hide();
            $(".inputBox4").hide();
            $(".inputBox3").hide();
            $(".inputBox").hide();
            const name = event.target.value
            const parent = this.state.parent;
            if (parent && name != '') {
                axios.post('http://2eaa0829.ngrok.io/menu', {
                    name,
                    parent
                }).then(response => {
                    this.setState({ parentData: response.data })
                    axios.get('http://2eaa0829.ngrok.io/menu', {
                        responseType: 'json'
                    }).then(response => {
                        this.setState({ data: response.data })
                    });
                })
            } else {
                axios.post('http://2eaa0829.ngrok.io/menu', {
                    name
                }).then(response => {
                    this.setState({ parentData: response.data })

                    axios.get('http://2eaa0829.ngrok.io/menu', {
                        responseType: 'json'
                    }).then(response => {
                        this.setState({ data: response.data })
                    });
                })
            }
            $("#parent1").val('');
            $("#parent").val('');
            $("#child").val('');
        }

    }

    componentDidMount() {
        $(".inputBox").hide();
        $(".inputBox2").hide();

        $(".navbar-brand").click(function () {
            $(".left-nav").toggleClass("hideShow-nav");
            $(".left-nav2").toggleClass("hideShow-nav");
            $('.Bodypart-wrapper').toggleClass("ml-lg-386");
            $('.Bodypart-wrapper').toggleClass("ml-40r");
        });
        $(".xs-button").click(function () {
            $(".left-nav").toggleClass("hideShow-nav-small");
            $(".left-nav2").toggleClass("hideShow-nav-small");
            $('.Bodypart-wrapper').toggleClass("ml-386");
        });
        $('.dropdown-menu').on('click', function (e) {
            if ($(this).hasClass('dropdown-menu-form')) {
                e.stopPropagation();
            }
        });
        $(".add-input-box").click(function () {
            $(".inputBox").toggle();
        });
        $(document).on("keypress", "input", function (e) {
            if (e.which == 13) {
                $(".inputBox2").hide();
            }

        })
        $(document).on("click", ".commonCLassUl a", function (e) {
            $(this).next().next().slideToggle();
            $(this).toggleClass("active");
            $(this).children().toggleClass("rotate");
        })
    }

    addInputBox = () => {
        $(".inputBox2").toggle();
    }
    addInputBox3 = () => {
        $(".inputBox3").toggle();
    }
    addInputBox4 = () => {
        $(".inputBox4").toggle();
    }


    render() {
        return (
            <div className="left-nav">

                <div className="tab-content">
                    <div className="tab-pane fade active in" id="home">
                        <div className="row top-toolbar">
                            <div className="col-sm-12">
                                <div className="pull-left ptb-15">
                                    <span>All</span>
                                </div>
                                <div className="pull-right">
                                    <ul className="title-action-icons check-val-input">
                                        <li><input type="text" name="name" id="parent" onKeyPress={this.handleChange} className="inputBox" /></li>
                                        <li className="active">
                                            <a href="javascript:void(0)" title="create container" className="add-input-box"><i className="fas fa-plus"></i></a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i className="fas fa-expand"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)"><i className="fas fa-angle-double-left"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row plr-30" >
                            <div className="col-sm-12">
                                <div id="accordian">
                                    <ul className="sideNav-menu-items-list-wrapper ">
                                        {
                                            this.state.data.map((res) =>
                                                <li className="commonCLassUl" key={res._id}>
                                                    <a href="javascript:void(0)" onClick={this.handleGetId.bind(this, res._id)} className="parentClick"><i className="fas fa-angle-right"></i>{res.name}</a>
                                                    <span className="pull-right acordion-icon-btn-wrapper">
                                                        <button type="button"><i className="far fa-clone"></i></button>
                                                        <span className="pull-right acordion-icon-btn-wrapper">
                                                            <button type="button" title="create item" className="add-btn" onClick={this.addInputBox4}><i className="fas fa-plus"></i></button>
                                                            <button type="button"><i className="fas fa-clone"></i></button>
                                                            <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                        </span>
                                                        <input type="text" className="inputBox4" name="name" id="parent1" onKeyPress={this.handleChange} />
                                                    </span>
                                                    <ul className="child1">
                                                        {
                                                            this.state.data && res.children ? res.children.map(child =>
                                                                <li className="commonCLassUl" key={child._id}>
                                                                    <a href="javascript:void(0)" onClick={this.handleGetId.bind(this, child._id)} ><i className="fas fa-angle-right"></i>{child.name}</a>
                                                                    <span className="pull-right acordion-icon-btn-wrapper check-val-input">
                                                                        <input type="text" name="name" id="child" onKeyPress={this.handleChange} className="inputBox3" />
                                                                        <button type="button" onClick={this.addInputBox3}><i className="fas fa-plus"></i></button>
                                                                        <button type="button"><i className="fas fa-clone"></i></button>
                                                                        <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                                    </span>
                                                                    <ul className="child2">
                                                                        {
                                                                            this.state.data && res.children && child.children ? child.children.map(child1 =>
                                                                                <li className="commonCLassUl check-val-input" key={child1._id}>
                                                                                    <a href="javascript:void(0)" onClick={this.handleGetId.bind(this, child1._id)} >{child1.name}</a>

                                                                                    <ul>
                                                                                        {
                                                                                            this.state.data && res.children && child.children && child1.children ? child.children.map(child =>
                                                                                                <li key={child._id}><a href="javascript:void(0)">{child.name}</a></li>
                                                                                            ) : null
                                                                                        }

                                                                                    </ul>
                                                                                </li>
                                                                            ) : null
                                                                        }
                                                                    </ul>
                                                                </li>
                                                            ) : null
                                                        }
                                                    </ul>
                                                </li>
                                            )
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}


export default LrnrAll;