import React, { Component } from 'react';
import './App.css';
import Layout from './components/Layout/Layout';
import LrnrBuilder from './containers/LrnrBuilder/LrnrBuilder';
import Header from '../src/components/Navigation/Header/Header';
 
class App extends Component {

    state = { 
      checked: false,
      cssChange:''
    }
    handleCheck = this.handleCheck.bind(this);
  
    
  handleCheck(e){
    
    this.setState({checked: e.target.checked})    
    if(e.target.checked){
      this.setState({cssChange:require('./assets/css/dark.css')})
    }else{
        window.location.reload(this.setState({cssChange:require('./assets/css/light.css')}));
    }
  }


  componentDidMount() {
    if(this.state.checked === false){
      require('./assets/css/light.css')
    }
  }

  
  render() {
    return (
      <div>
        <Header  
          handleCheck={this.handleCheck}
        />
        <Layout>
          <LrnrBuilder />
        </Layout>
      </div>
    );
  }
}

export default App;
