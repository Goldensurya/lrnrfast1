import React, { Component } from 'react';
import Aux from '../../hoc/Aux/Aux';
import LrnrControls from '../../components/Lerners/LrnrControls/LrnrControls';

class LrnrBuilder extends Component {
    
  render() {
    return (
        <Aux>
            <LrnrControls />
        </Aux>
    );
  }
}

export default LrnrBuilder;